import java.util.Scanner;

/*
*create game that ask player1 for a word
*player2 has 6 chances to guess to word
*/
public class Hangman {
	
	/*
	*ask for player1 to enter a word
	*call method runGame using the word as parameter
	*/
	public static void startGame() {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("Enter a four-letter word");
		String mysteryWord = reader.next();
		
		while (mysteryWord.length() != 4) {
			System.out.println("Enter a four-letter word");
			mysteryWord = reader.next();
		}
		
		runGame(mysteryWord);
	}
	
	/*
	*parameter: String word & char letter
	*find if letter is in word
	*return letter index in word
	*/
	public static int isLetterInWord(String word, char letter) {
		int count = 0;
		
		//compare letter to each char in word
		while (count <= 3) {
			
			/*
			*compared letter in uppercase to 1 char of word in uppercase
			*return letter index in word
			*/
			if (toUpperCase(word.charAt(count)) == toUpperCase(letter)) {
				return count;
			}
			count++;
		}
		
		//default value if letter is not in word
		return -1;
	}
	
	/*
	*parameter: char letter
	*return letter converted to uppercase
	*/
	public static char toUpperCase(char letter) {
		
		//if letter is in lowercase, turn letter to uppercase
		if (letter >= 97 && letter <= 122) {
			letter -= 32;
		}
		
		return letter;
	}
	
	/*
	*parameter: String mysteryWord, boolean letters
	*reveal known letters' place in mysteryWord
	*/
	public static void printWork(String mysteryWord, boolean[] letters) {
		
		char [] result = new char[] {'_', '_', '_', '_'};
		
		System.out.print("Your result is ");
		
		//change char index value to their letter if their respective boolean value is true
		for (int i = 0; i < letters.length; i++) {
			if (letters[i]) {
				result[i] = mysteryWord.charAt(i);
			}
			System.out.print(result[i]);
		}
		
		System.out.println(".");
	}
	
	/*
	*parameter: String mysteryWord
	*ask player2 to guess a letter
	*call printWork after each guess
	*player2 wins game if mysteryWord is guessed before making 6 mistakes
	*/
	public static void runGame(String mysteryWord) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		boolean[] letterGuessed = new boolean[mysteryWord.length()];
		for (int i = 0; i < letterGuessed.length; i++) {
			letterGuessed[i] = false;
		}
		
		int mistake = 0;
		
		//ask player2 for a guess
		//if player2 makes 6 mistake or find all letters, exit loop
		while (mistake < 6 && !(letterGuessed[0] && letterGuessed[1] && letterGuessed[2] && letterGuessed[3])) {
			System.out.println("Enter a letter");
			char guess = reader.next().charAt(0);
			int letterIndex = isLetterInWord(mysteryWord, guess);
			
			//if letterIndex is lower than 0, increment mistake value
			//else change char boolean value according to the letterIndex value
			if (letterIndex >= 0 && letterIndex < mysteryWord.length()) {
				letterGuessed[letterIndex] = true;
			} else {
				mistake++;
			}
			
			printWork(mysteryWord, letterGuessed);
		}

		//display result of game
		if (mistake == 6) {
			System.out.println("The word was " + mysteryWord + ". Better luck next time.");
		} else {
			System.out.println("You win!");
		}
	}
}