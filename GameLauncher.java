import java.util.Scanner;

/*
*Ask player to choose a game to play and redirect to said game
*/
public class GameLauncher{
	public static void main(String[] args) {
		System.out.println("Hello! Please select the game you want to play: ");
		System.out.println("1: Hangman");
		System.out.println("2: Wordle");
		
		//store a game id
		int gameId = checkId();
		launchGame(gameId);
	}
	
	//check if player chose a valid game id
	public static int checkId() {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		int gameId = reader.nextInt();
		while (gameId <= 0 || gameId > 2) {
			System.out.println("! Please input another number!");
			gameId = reader.nextInt();
		}
		
		return gameId;
	}
	
	//redirect to game chosen
	public static void launchGame(int id) {
		if (id == 1) {
			System.out.println("You've chosen the Hangman game");
			Hangman.startGame();
		} else /*if (gameId == 2)*/ {
			System.out.println("You've chosen the Wordle game");
			Wordle.runGame();
		}
	}
}