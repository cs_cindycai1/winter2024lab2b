import java.util.Scanner;
import java.util.Random;


/*
*creat wordle game
*player guess the five-letter word
*five chaces
*/
public class Wordle {
	
	/*
	*no parameter
	*return random word from an array containing 20 strings
	*/
	public static String generateWord() {
		Random randNum = new Random();
		
		String [] words = new String [] {"GROWN", "SWORD", "SCORE", "GRADE", "FRAIL", "WIDEN", "PRIZE", "JOUST", "BLAST", "VITAL", "POINT", "GRAPE", "MINER", "HOVER", "QUOTE", "DRAWN", "SHARD", "CRISP", "WHINE", "FEAST"};
		
		return words[randNum.nextInt(21)];
	}
	
	/*
	*PARAMETER: secret word & 1 char from player's guess
	*return true if word contains char
	*return false if word does not contains char
	*/
	public static boolean letterInWord(String word, char letter) {
		
		/*
		*compare char letter to each char of word
		*/
		for (int i = 0; i < word.length(); i++) {
			
			/*
			*compare if letter exist at word's index
			*/
			if (word.charAt(i) == letter) {
				return true;
			}
		}
		
		return false;
	}
	
	/*
	*PARAMETER: secret word, 1 char from player's guess & int representing letter index 
	*return true if char is in word and at the right place
	*return false if char is not in word or char is not in the right index 
	*/
	public static boolean letterInSlot(String word, char letter, int position) {
		
		/*
		*calls method letterInWord to check if letter is in word
		*check if index of letter in the word is the same as the int position
		*/
		if (letterInWord(word, letter) && word.indexOf(letter) == position) {
			return true;
		}
		
		return false;
	}
	
	/*
	*PARAMETER: String secret word & String player's guess
	*return String array containing color representing result of player's guess compared to answer
	*/
	public static String[] guessWord(String answer, String guess) {
		String [] colours = new String[5];
		
		/*
		*for each element in the array
		*gives the element a colour value if the letter is in the right place, in the wrong place or not in word
		*/
		for (int i = 0; i < colours.length; i++) {
			
			/*
			*calls method letterInWord
			*if true, go check if letter is in right index
			*if false, assign "white" as array element value
			*/
			if (letterInWord(answer, guess.charAt(i))) {
				
				/*
				*calls letterInSlot to check if letter is in the right index
				*if true, assign "green" as array element value
				*if false, assign "yellow" as array element value
				*/
				if (letterInSlot(answer, guess.charAt(i), i)) {
					colours[i] = "green";
				} else {
					colours[i] = "yellow";
				}
			} else {
				colours[i] = "white";
			}
		}
		
		return colours;
	}
	
	/*
	*PARAMETER: String player's guess & String array containing the letters' colour
	*print the result of player's guess with the color representing if letter is in the right place, wrong place, or not in the secret word
	*/
	public static void presentResults(String word, String[] colours) {
		String[] result = new String [5];
		
		/*
		*for each value in String array result, assign a ASNI text corresponding to the array colours's value
		*/
		for (int i = 0; i < result.length; i++) {
			
			/*
			*check value of String array Colours at specific index
			*assign text color according to array Colour index value
			*/
			if (colours[i].equals("green")) {
				result[i] = "\u001B[32m";
			} else if (colours[i].equals("yellow")) {
				result[i] = "\u001B[33m";
			} else {
				result[i] = "\u001B[0m";
			}
		}
		
		String colouredText = "";
		
		/*
		*create String containing coloured text
		*/
		for (int j = 0; j < result.length; j++) {
			colouredText += (result[j] + word.charAt(j));
		}
		
		colouredText += "\u001B[0m";

		System.out.println(colouredText);
	}
	
	/*
	*no parameter
	*return player's guess in uppercase letter
	*ask player to enter a five-letter guess
	*/
	public static String readGuess() {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		System.out.println("Enter your guess:");
		String guess = reader.next();
		
		/*
		*check if guess contains five letters
		*ask player to change word if guess is not five letter long
		*/
		while (guess.length() != 5) {
			System.out.println("Please enter a valid guess:");
			guess = reader.next();
		}
		
		String uppercaseWord = "";

		/*
		*convert every letter in word to uppercase
		*/
		for (int i = 0; i < guess.length(); i++) {
			char letterToUppercase = guess.charAt(i);
			
			/*
			*if letter is in lowercase, convert letter to uppercase
			*/
			if (guess.charAt(i) >= 97 && guess.charAt(i) <= 122) {
				letterToUppercase -= 32;
			}
			uppercaseWord += letterToUppercase;
		}
		
		return uppercaseWord;
	}
	
	/*
	*PARAMETER: secret word generated from method generateWord
	*call method readGuess
	*call method presentResults
	*display if the player has the correct answer or not
	*gives five chances
	*if player guess correctly within five chance, display "You win"
	*display "You lose" and the secret word if player do not guess correctly within five guesses
	*/
	public static void runGame() {
		
		String answer = generateWord();
		
		int guessNum = 0;
		
		/*
		*count number of guesses
		*display if the player's guess is correct or wrong
		*/
		while (guessNum < 5) {
			guessNum++;
			
			String guess = readGuess();
			String[] colouredText = guessWord(answer, guess);
			presentResults(guess, colouredText);
			
			/*
			*check if every value of String array colouredText is "green"
			*if true, display "You win" and exit the loop
			*if false, display "try again"
			*/
			if (colouredText[0].equals("green") && colouredText[1].equals("green") && colouredText[2].equals("green") && colouredText[3].equals("green") && colouredText[4].equals("green")) {
				System.out.println("You win");
				guessNum = 6;
			} else if (guessNum != 5){
				System.out.println("Try again");
			}
		}
		
		/*
		*display "You lose" and the correct answer if player made five wrong guesses
		*/
		if (guessNum == 5) {
			System.out.println("You lost");
			System.out.println("The answer was " + answer);
		}
	}
}